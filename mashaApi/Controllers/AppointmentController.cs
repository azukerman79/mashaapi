﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Model;
using Model.PartialClasses;
namespace mashaApi.Controllers
{
    public class AppointmentController : ApiController
    {
        private MashaDBContext db = new MashaDBContext();

        public IHttpActionResult GetAppointmentsForPupil(int pupil_id)
        {
            try
            {
                var pupilsAppointments = db.appointments.Where(m => m.pupil_id == pupil_id).Select
               (m => new appointmentForStudent
               {
                   appointment_id = m.appointment_id,
                   appointment_date = m.appointment_date,
                   appointment_time = m.appointment_time,
                   appointment_title = m.appointment_title,
                   instructor_id = m.instructor_id,
                   instructors_first_name = m.instructor.user.first_name,
                   instructors_last_name = m.instructor.user.last_name,
                   instructors_photo = m.instructor.user.photo,
                   pupil_id = m.pupil_id,
                   pupil_first_name = m.pupil.user.first_name,
                   pupil_last_name = m.pupil.user.last_name,
                   pupil_photo = m.pupil.user.photo,
                   was_attendant = m.is_pupil_attendant,


               }
               );
                if (pupilsAppointments != null)
                    return Ok(pupilsAppointments);
                throw null;
            }
            catch
            {
                return NotFound();
            }

        }
    }
}