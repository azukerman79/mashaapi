﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Model;
using Model.PartialClasses;

namespace mashaApi.Controllers
{
    public class wordController : ApiController
    {
        private MashaDBContext db = new MashaDBContext();
        private int known_word_count = 2;

        private IEnumerable GetWordStatisticsForPupilByLengauge(List<word_pupil> word_pupils)
        {
            int CountOfunknowWordsCount=0;
            int CountOfknowWordsCount=0;
            int  CountOfwordsThatWerentPlayed=0;
            int CountOfTotalWords = word_pupils.Count();
            foreach (word_pupil wp in word_pupils)
            {
                if (wp.word_currect_number >= known_word_count)
                    CountOfknowWordsCount++;
                else if (wp.word_currect_number >= 0 && wp.word_number_of_times_seen>0)
                    CountOfunknowWordsCount++;
                if (wp.word_number_of_times_seen == 0)
                    CountOfwordsThatWerentPlayed++;
            }
            var result = new { CountOfunknowWordsCount = CountOfunknowWordsCount,
                CountOfknowWordsCount = CountOfknowWordsCount,
                CountOfwordsThatWerentPlayed = CountOfwordsThatWerentPlayed,
                CountOfTotalWords = CountOfTotalWords
            };
            yield return result;

        }
        [Route("api/getWordStatisticsForPupil")]
        public IHttpActionResult getWordStatisticsForPupil( int pupil_id)
        {

            try
            {
                List<word_pupil> wordsForHebrew = db.word_pupil.Where(wp => wp.word.word_language == true).ToList();

                var statisticsForHebrew = GetWordStatisticsForPupilByLengauge(wordsForHebrew);

                List<word_pupil> wordsForEnglish = db.word_pupil.Where(wp => wp.word.word_language == false).ToList();

                var statisticsForEnglish = GetWordStatisticsForPupilByLengauge(wordsForEnglish);
                var statistics = new {
                    statisticsForHebrew = statisticsForHebrew,
                    statisticsForEnglish = statisticsForEnglish
                };
                return Ok(statistics);
            }
           catch
            {
                return NotFound();
            }
            
            
        }

        [Route("api/updateWordPupilAssiciation")]
        public IHttpActionResult Get_updateWordPupilAssiciation_API(int word_id,int pupil_id, string word_assiciation)
         {
            try
            {
                var wp = db.word_pupil.Where(w => w.pupil_id == pupil_id && w.word_id == word_id).FirstOrDefault();
                if (wp != null)
                {
                    wp.word_assiciation = word_assiciation;
                    db.SaveChanges();
                    return Ok();
                }
                throw null;
            }
            catch
            {
                return NotFound();
            }
           
           
        }


        [Route("api/getQuestionsForPupil")]
        public IHttpActionResult getQuestionsForPupil_API(int pupil_id, int number_of_words= 4)
        {
            bool leanguage = true;
            var words = db.word_pupil.Where(wp=>wp.pupil_id==pupil_id && wp.word.word_language==leanguage)
                                     .ToList()
                                      .OrderBy(x => Guid.NewGuid()).Take(number_of_words).ToList();
            List<Question> questions = new List<Question>();
            int questionNumber = 1;
            foreach(word_pupil wp in words)
            {   
                var question = getQuestionBywordID(wp.word_id,questionNumber);
                question.WordAssociation = wp.word_assiciation;
                questions.Add(question);
                questionNumber++;
            }

            return Ok(questions);

        }

        #region utilityMethods
        private Question getQuestionBywordID(int wordID,int questionNum)
        {

            var word = db.words.Where(w => w.word_id == wordID).FirstOrDefault();
            if (word == null)
            {
                return null;
            }

            var listOfAnswers = getRandomAnswers(word.word_id, word.word_language, questionNum);
            Question questionForTheWord = new Question(word, questionNum);

            questionForTheWord.Meanings = listOfAnswers;
            questionForTheWord.addAnswer(new Answer(word.word_meaning_context,true,questionNum));
            questionForTheWord.shuffleAnswers();

            return questionForTheWord;
        }
        #endregion


        [Route("api/InsertWord")]
      
        [Route("api/removeWordByword_id")]
        public IHttpActionResult removeWordByword_id(int word_id)
        {
                try
                {
                var word_to_remove = db.words.Where(w => w.word_id == word_id).FirstOrDefault();
                db.words.Remove(word_to_remove);
                    return Ok(word_id);
                }
                catch
                {
                    return NotFound();
                }
           
        }

        [Route("api/addWordToAllPupils")]
        public IHttpActionResult addWordToAllPupils(int word_id)
        {
            try
            {
                var pupil_ids = db.pupils.Where(p=>p.is_active==true).Select(p => p.pupil_id);
                foreach (int pupil_id in pupil_ids)
                    db.word_pupil.Add(new word_pupil { pupil_id = pupil_id, word_id = word_id });
                return Ok();
               
            }
            catch
            {
                return null;
            }
        }
        [Route("api/GetAllwords")]
        public IEnumerable<word> GetAllwords()
        {
            try
            {
                var words = db.words;
                return words;
            }
            catch
            {
                return null;
            }

        }

        private List<Answer>  getRandomAnswers(int wordID,bool lenguage, int questionNum)
        {
            try {
                var answers = db.words.Where(m => m.word_id != wordID && m.word_language == lenguage)
                                      .ToList()
                                       .OrderBy(x => Guid.NewGuid()).Select(m => m.word_meaning_context).Take(3).ToList();
                List<Answer> listOfAnswers = new List<Answer>();
                foreach (string s in answers)
                    listOfAnswers.Add(new Answer(s,false, questionNum));
                return listOfAnswers;
            }
            catch
            {
                return null;
            }
            
           
        }
       
    }
 
}

