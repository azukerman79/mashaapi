﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Model;
using Model.PartialClasses;
namespace mashaApi.Controllers
{
    public class StudentsController: ApiController
    {
        private MashaDBContext db = new MashaDBContext();

        public IHttpActionResult GetStudentsOfInstructor(int instructor_id)
        {




            try
            {
                var students = db.users.Where(u => u.pupil.instructor_id == instructor_id && u.pupil.is_active == true).Select
                 (u => new StudentInfo
                 {
                     first_name = u.first_name,
                     email = u.email,
                     last_name = u.last_name,
                     photo = u.photo,
                     user_id = u.user_id
                 }
                 );
                if (students != null)
                    return Ok(students);
                throw null;
            }
            catch
            {
                return NotFound();
            }


            

          
        }

    }
}