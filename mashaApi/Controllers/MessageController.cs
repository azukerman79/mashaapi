﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Model;
using Model.PartialClasses;
namespace mashaApi.Controllers
{
    public class MessageController : ApiController
    {
        private MashaDBContext db = new MashaDBContext();

        public IHttpActionResult GetMessagesForPupil(int pupil_id)
        {
            try
            {
                var msgs = db.messages.Where(m => m.pupil_id==pupil_id && m.task==null).Select
               (m => new MessegeForPupil
               {
                   message_id = m.message_id,
                   pupil_id = pupil_id,
                   instructor_id = m.instructor_id,
                   message_date = m.message_date,
                   message_description = m.message_description,
                   message_title = m.message_title,
                   instructors_photo = m.instructor.user.photo,
                   instructors_first_name = m.instructor.user.first_name,
                   instructors_last_name = m.instructor.user.last_name

               }
               );
                if (msgs != null)
                    return Ok(msgs);
                throw null;
            }
            catch
            {
                return NotFound();
            }
          
        }
        public IHttpActionResult GetMessagesForInstructor(int instructor_id)
        {
            try
            {
                var msgs = db.messages.Where(m => m.instructor_id == instructor_id && m.task == null).Select
               (m => new MessegeForPupil
               {
                   message_id = m.message_id,
                   instructor_id = instructor_id,
                   pupil_id = m.pupil_id,
                   message_date = m.message_date,
                   message_description = m.message_description,
                   message_title = m.message_title,
                   pupils_photo = m.pupil.user.photo,
                   pupils_first_name = m.pupil.user.first_name,
                   pupils_last_name = m.pupil.user.last_name

               }
               );
                if (msgs != null)
                    return Ok(msgs);
                throw null;
            }
            catch
            {
                return NotFound(); 
            }

        }
    }
}