﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class MashaDBContext : DbContext
    {
        public MashaDBContext()
            : base("name=MashaDBContext")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<user> users { get; set; }
        public virtual DbSet<appointment> appointments { get; set; }
        public virtual DbSet<manager> managers { get; set; }
        public virtual DbSet<message> messages { get; set; }
        public virtual DbSet<pupil> pupils { get; set; }
        public virtual DbSet<word> words { get; set; }
        public virtual DbSet<word_pupil> word_pupil { get; set; }
        public virtual DbSet<instructor> instructors { get; set; }
        public virtual DbSet<task> tasks { get; set; }
    }
}
