﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.PartialClasses
{
    public partial class Question
    {
        public List<Answer> Meanings { get; set; }
        public string Word { get; set; }
        public int WordID { get; set; }
        public string Answer { get; set; }
        public string WordAssociation { get; set; }
        public int QuestionNum { get; set; }

        public Question(word word,int questionNum)
        {
            this.QuestionNum = questionNum;
            this.Word = word.word_context;
            this.WordID = word.word_id;
            this.Answer = word.word_meaning_context;
            Meanings = new List<Answer>();
        }
        public void addAnswer(Answer answer)
        {
            if (answer != null)
                this.Meanings.Add(answer);
        }
        public void shuffleAnswers()
        {
            Random r = new Random();
            this.Meanings = this.Meanings.OrderBy(a => r.Next()).ToList();
        }
       
    }

    
}
