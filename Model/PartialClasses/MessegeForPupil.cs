﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.PartialClasses
{
    public class MessegeForPupil
    {
        public int message_id { get; set; }
        public string message_title { get; set; }
        public string message_description { get; set; }
        public System.DateTime message_date { get; set; }
        public int instructor_id { get; set; }
        public int pupil_id { get; set; }
        public string instructors_photo { get; set; }
        public string instructors_first_name { get; set; }
        public string instructors_last_name { get; set; }

        public string pupils_photo { get; set; }
        public string pupils_first_name { get; set; }
        public string pupils_last_name { get; set; }

    }
}
