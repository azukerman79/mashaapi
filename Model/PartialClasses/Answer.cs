﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.PartialClasses
{
    public class Answer
    {
        public int QuestionNum
        {
            get;
            set;
        }

        public string Meaning
        {
            get;
            private set;
        }

        public bool IsCorrect
        {
            get;
            private set;
        }

        public Answer(string meaning, bool isCorrect, int questionNum)
        {
            Meaning = meaning;
            IsCorrect = isCorrect;
            QuestionNum = questionNum;
        }
    }
}
