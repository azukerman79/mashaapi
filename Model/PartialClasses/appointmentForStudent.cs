﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.PartialClasses
{
    public class appointmentForStudent
    {
        public int appointment_id { get; set; }
        public string appointment_title { get; set; }
        public System.DateTime appointment_date { get; set; }
        public System.TimeSpan appointment_time { get; set; }
        public Nullable<bool> was_attendant { get; set; }
       
        public int pupil_id { get; set; }
        public string pupil_photo { get; set; }
        public string pupil_first_name { get; set; }
        public string pupil_last_name { get; set; }
        public int instructor_id { get; set; }
        public string instructors_photo { get; set; }
        public string instructors_first_name { get; set; }
        public string instructors_last_name { get; set; }
        

    }
}
