﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.PartialClasses
{
    public partial class UserResult
    {
        public int user_id { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string password { get; set; }
        public string photo { get; set; }

        public bool isManager  { get; set; }
        public bool isPupil { get; set; }
        public bool isInstructor { get; set; }

        public UserResult(user u)
        {
            this.user_id = u.user_id;
            this.email = u.email;
            this.first_name = u.first_name;
            this.last_name = u.last_name;
            this.password = u.password;
            this.photo = u.photo;
            if (u.instructor != null)
                isInstructor = true;
            else if (u.manager != null)
                isManager = true;
            else if(u.pupil!=null)
            {
                isPupil = true;
            }
           
        }
    }
}
